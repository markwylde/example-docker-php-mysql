## Create a new droplet
Select `Container Distrubutions > CoreOS` as the operating system

## Install docker-compose
Install docker-compose
```bash
sudo su -
mkdir -p /opt/bin
curl -L https://github.com/docker/compose/releases/download/1.23.1/docker-compose-`uname -s`-`uname -m` > /opt/bin/docker-compose
chmod +x /opt/bin/docker-compose
```

## Clone example project
Fetch all the files for the example LAMP app

```bash
git clone https://gitlab.com/markwylde/example-docker-php-mysql.git
cd example-docker-php-mysql
```

## Run project
Use docker-compose to launch the project

```bash
docker-compose up
```

## Visit site
Navigate your browser to:
```
https://IP_ADDRESS:8100
```