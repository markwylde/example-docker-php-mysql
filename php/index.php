<?php
  $servername = "db";
  $dbname = getenv('DB_NAME');
  $username = getenv('DB_USERNAME');
  $password = getenv('DB_PASSWORD');

  $conn = new mysqli($servername, $username, $password, $dbname);

  # Connect to MySQL
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  # Create messages table
  $sql = "CREATE TABLE `messages` ( `id` INT NOT NULL AUTO_INCREMENT , `message` TEXT NOT NULL , PRIMARY KEY (`id`));";
  $result = $conn->query($sql);
  if (!$result) {
    printf("Errormessage: %s\n", $conn->error);
  }

  # Create a new message
  $sql = "INSERT INTO messages (message) VALUES ('hello from " . date('m/d/Y h:i:s a', time()) . "')";
  $result = $conn->query($sql);

  # Find all the messages
  $sql = "SELECT id, message FROM messages ORDER BY id desc";
  $result = $conn->query($sql);
?>

<html>
  <head>
    <title>Example PHP MySQL</title>
  </head>

  <body>
    <hr />
      <?php
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            echo "id: " . $row["id"]. " - Message: " . $row["message"] . "<br>";
          }
        } else {
          echo "0 results";
        }
      ?>
  </body>
</html>

<?php
  $conn->close();
?>